#include <NewPing.h>
#define SONAR_NUM 6
#define MAX_DIST 200

int ledPin[SONAR_NUM] = {3,5,6,9,10,11};
int sonarPin[SONAR_NUM] = {2,4,7,8,12,13};

int dist = 0;
int i = 0;

NewPing sonar[SONAR_NUM] = {
  NewPing(2,2,MAX_DIST),
  NewPing(4,4,MAX_DIST),
  NewPing(7,7,MAX_DIST),
  NewPing(8,8,MAX_DIST),
  NewPing(12,12,MAX_DIST),
  NewPing(13,13,MAX_DIST)
};
void setup() {
  // put your setup code here, to run once:
  for (i=0; i<SONAR_NUM; i++) {
    pinMode(ledPin[i], OUTPUT);
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  delay(50);
  for (i=0; i<SONAR_NUM; i++) {
      dist = sonar[i].ping_cm();
    if (dist < 8) {
      digitalWrite(ledPin[i], HIGH);
    } else {
      digitalWrite(ledPin[i], LOW);
    }
  }
}
