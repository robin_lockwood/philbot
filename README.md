# README #

This is an arduino project.  It's run out of an IDE at the moment.  Will update when the time arrives.

### What is this repository for? ###

* Ground up version of a search and destroy robot
* 0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Get an Arduino with PWM and some sensors
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Owner and Developer: Robin
* Mentor: Michael Cassens